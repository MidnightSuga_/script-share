on error resume next

' ********** Get computer name from the user
strComputer=inputbox("Enter Computer Name: ", "Current User Mapped Drives")

' ********** Define constants
Const HKEY_USERS = &H80000003

' ********** Blank the report message
strMsg = ""

' ********** Set objects 
Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set objWbem = GetObject("winmgmts:")
Set objRegistry = GetObject("winmgmts://" & strComputer & "/root/default:StdRegProv")

if err.number = "-2147217375" then
	' Do nothing
else

	' ********** Check to make sure the computer exists on the network.
	Select Case err.number
		Case 462
			strWarn=MsgBox("Unable to connect to " & strComputer & ".", 48, "System Information Checker")
		Case -2147217394
			strWarn=MsgBox(strComputer & " is not a valid name.", 48, "System Information Checker")
		Case 70
			strWarn=MsgBox(strComputer & " has denied access.", 48, "System Information Checker")
    	Case Else

	' ********** Get the current user from Explorer  

	Set colProc = objWmiService.ExecQuery("Select Name from Win32_Process" & " Where Name='explorer.exe'")

	If colProc.Count > 0 Then
		For Each oProcess In colProc
			oProcess.GetOwner sUser, sDomain
		Next
	End If

	' ********** Print user and computer
	strMsg = strMsg & "    User: " & sUser & VbCrLf
	strMsg = strMsg & "Computer: " & strComputer & VbCrLf & VbCrLf
	

	' ********** Loop through the HKEY_USERS hive until the currently logged on user is matched
	lngRtn = objRegistry.EnumKey(HKEY_USERS, "", arrRegKeys)
	
	For Each strKey In arrRegKeys
		If UCase(strKey) = ".DEFAULT" Or UCase(Right(strKey, 8)) = "_CLASSES" Then
		Else
			Set objSID = objWbem.Get("Win32_SID.SID='" & strKey & "'")
			If objSID.accountname = sUser Then 
				regpath2enumerate = strkey & "\Network" 'strkey is the SID
				objRegistry.enumkey hkey_users, regpath2enumerate, arrkeynames
				
				If Not (IsEmpty(arrkeynames)) Then
					For Each subkey In arrkeynames
						regpath = strkey & "\Network\" & subkey
						regentry = "RemotePath"
						objRegistry.getstringvalue hkey_users, regpath, regentry, dapath
						strMsg = strMsg & subkey & ":" & vbTab & dapath & VbCrLf
					Next
				End If
			End If
		End If
	Next


		' ********** Check for the existence of the "SysInfoCheck" folder then
		' ********** write the file to disk.
		strDirectory = "C:\SysInfoCheck"
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		If objFSO.FolderExists(strDirectory) Then
    			' Procede
		Else
    			Set objFolder = objFSO.CreateFolder(strDirectory)
		End if

		' ********** Calculate date serial for filename **********
		intMonth = month(now)
		if intMonth < 10 then
			strThisMonth = "0" & intMonth
		else
			strThisMonth = intMOnth
		end if
		intDay = Day(now)
		if intDay < 10 then
			strThisDay = "0" & intDay
		else
			strThisDay = intDay
		end if
		strFilenameDateSerial = year(now) & strThisMonth & strThisDay

		Set objFile = objFSO.CreateTextFile(strDirectory & "\" & strComputer & "_" & sUser & "_MappedDrives" & "_" & strFilenameDateSerial & ".txt",True)	
		objFile.Write strMsg & vbCrLf

		' ********** Ask to view file
		strFinish = "Finished collecting mapped drives for computer: " & strComputer & "." & VbCrLf & VbCrLf & "View file?"
		strAnswer=MsgBox(strFinish, 68, "System Information Checker")
		if strAnswer = 6 then
    			Set objShell = CreateObject("WScript.Shell")
    			objShell.run strDirectory & "\" & strComputer & "_" & sUser & "_MappedDrives" & "_" & strFilenameDateSerial & ".txt"
		end if

	end select

end if